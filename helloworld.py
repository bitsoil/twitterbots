#python3.6

print("Hello world!")
print("Python assigns types to different kinds of data, and interacts with the types differently")
print("Anything between quotes is a string")
print("Numbers are integers")
print("You can use the print command to evaluate code")
print(2)
print(type(2))

#You comment code like this. 
#You can assign variables

thng = "Hi"

# And ask python what "kind" or "type" they are

print(type(thng))

num = 2
print(type(num))


# Python allows you to create lists of things -- these could be strings, integers, anything
lst = ['apple', 'banana', 'orange']
print(type(lst))
# or a list of lists

lst2 = [lst, ['hi','again']]
print(lst2)

lst3 = ['apple', 'grape', 'pineapple']


# a for loop can cycle through the "things" in a list
for thing in lst: 
	# We use %s to add a variable to a string
	print("Hello %s" % thing)
	# we use if statements to evaluate variables too
	if thing in lst3:
		# if this is TRUE, then this sentence will print
		print("Yes, %s is in lst3" % thing)
		print(lst3)
	#elif allows us to try something else if our first if statement is FALSE
	elif thing == 'orange':
		#if this is true, then the following sentence will print
		print("It's not in lst3, but it is %s" % thing)
		# if this is not true either, then add another response
	else:
		print("Nope, it's not in lst3, and it's not an orange, it's %s!" % thing)

#dictionaries are more complex organisations of things
# they have a "key", or reference, and a "values". The first string in the grouping a:b is the key
# so in a:b, a is the key, an b is the value
# dictionaries hold many key:value pairs
dct = {"thing1":"apple", "thing2":"banana", "thing3":"orange"}
print(type(dct))


# a for loop, when called correctly, can cycle through the key:value pairs of a dictionary
for key, value in dct.items():
	# WE can use %s to add multiple variables to a string too 
	print("Hello, %s, you are called %s" % (key, value))
