import sys
import datetime
import time
import tweepy
import codecs
import csv
import shutil
import urllib
from random import randint

from datetime import datetime
import pandas as pd


#authorise bot and create api
consumer_key = 'bQwmuNToFenj708HVnq1o4wue'
consumer_secret = 'v6t0NmOsnApn478y0uZjSi8fGX31Uh5mcPdJfaVwMJBDuL1kOj'
access_token = '978925002119680000-C9kxlPMO7YfBFg2V3JLsDBGuBnImdp5'
access_token_secret = 'zDTFxJ0XRv4yU1ktglw87PrJwSrB3mJrZ4Mpsk87b3MTN'

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
twapi = tweepy.API(auth)


# optional: since when do we want to tweet? 
def get_third_trump_tweet():
	statuses = twapi.user_timeline(screen_name='realdonaldtrump')
	status = statuses[2]
	status_id = status.id
	return status_id



# run a seraching bot
def run_prospector_bot_tasks():
	print("we're here!")
	df = pd.DataFrame()

	# optional, if we want a "since when"
	tweet_id = get_third_trump_tweet()

	# import keywords0
	file_lines = 'prospector/data/keywords.txt'

	with open(file_lines) as f:
		content = f.readlines()
		content = [x.strip() for x in content] 

	print(content)
	
	# for keyword in keywords, search twitter
	for x in content: 
		print(x)
		#try: 
		# bring in 100 tweets per keyword
		tweets = twapi.search(q=x,count=100)
		print("Successfully harvested some tweets")
		print(tweets)
	

		# sanity check
		print(len(tweets))

		# for twt in collection of tweets
		for twt in tweets:
			print("hi")
			twt.text = twt.text.strip("\n")
			twt.text = twt.text.replace("\n", " ")
			twt.text = twt.text.replace(",", " ")
			twt.text = twt.text.replace("'", " ")
			print(twt.text)

			# set easy screenname variable
			twt.screen_name = twt.author._json['screen_name'].lstrip()

			# we're adding everything to a data-storage construction called a dataframe. This lets us easily write
			# to a csv file
			df = df.append({'tweet_id': twt.id, 'screen_name': twt.screen_name, 'tweet_text': twt.text, 'keyword': x}, ignore_index=True)
			
			# sanity check
			print(twt.screen_name)
			print(twt.text)
			print(twt.id)
			print(df.head())

	# write all of our tweets to a csv
	df.to_csv('prospector/data/tweets.csv', index=True, index_label='id')

if __name__ == "__main__":
	run_prospector_bot_tasks()
