from splinter import Browser
import time
from datetime import datetime
import random

# this script does the heavy lifting on the twitter websites
# opens and closes chrome with each run 

# we need to pretend like a human, and so we pause after every step for a random amount of time. 
def nap():
	timeDelay = random.randrange(4, 20)
	print("sleeping...")
	print(timeDelay)
	time.sleep(timeDelay)


#customise: 


data = {
	#consumer_key: 'consumer_key',
	#consumer_secret: 'consumer_secret',
	#access_key: 'access_key',
	#access_secret: 'access_secret',
	email: 'bot20@shush.systems',
	pw: 'no4JuLmRvkWTjjPXC75n',
	app_name: 'Demonstration Bot 2803',
	descriptrion: 'Demonstration Bot 2803 This description needs to be longer',
	url: 'https://bitsoil.tax'
}


# run it ! 
def run_scraper(data):
	# open incognito chrome
	#executable_path = {'executable_path':'/Users/cst/chromedriver'}

	browser = Browser('chrome', incognito=True)

	time.sleep(10)
	# get our result ready
	result = {}
	print(data)
	print("eeee")

	# first we login at this page which clears away all of the confusing stuff (because it's made to
	# redirect)
	url = "https://twitter.com/login?redirect_after_login=https%3A/apps.twitter.com/"
	browser.visit(url)
	time.sleep(10)

	# login
	el_user = browser.find_by_name('session[username_or_email]')
	print(el_user)
	print(data['email'])
	el_password = browser.find_by_name('session[password]')
	for el in el_user: 
		if el.has_class('js-username-field'):
			el.fill(data['email'])

	nap()

	for el in el_password: 
		if el.has_class('js-password-field'):
			el.fill(data['pw'])


	nap()

	# click login button
	el_login = browser.find_by_text('Log in')
	for el in el_login:
		if el.has_class('submit'):
			el.click()

	print("long nap")
	time.sleep(35)

	# once we're logged in, we visit the url that allows us to make a new app
	url2 = "https://apps.twitter.com/app/new"
	browser.visit(url2)

	# on page loads, etc, we take a longer nap
	print("long nap")
	time.sleep(12)


	# insert unique app name
	el_app_name = browser.find_by_id('edit-name')
	for el in el_app_name: 
		el.fill(data['app_name'])

	nap()

	# insert app description
	el_app_description = browser.find_by_id('edit-description')
	for el in el_app_description: 
		el.fill(data['description'])

	nap()

	# insert website
	el_app_website = browser.find_by_id('edit-url')
	for el in el_app_website:
		el.fill(data['url'])

	nap()

	#click agreement
	el_agreement = browser.find_by_id('edit-tos-agreement')
	for el in el_agreement: 
		el.check()

	nap()

	# submit first form
	el_submit = browser.find_by_id('edit-submit')
	for el in el_submit: 
		el.click()
	
	print("long nap")
	time.sleep(20)
	
	# find and click the "keys and access tokens" tab on the next page
	el_keys = browser.find_by_text('Keys and Access Tokens')
	for el in el_keys: 
		el.click()

	print("long nap")
	time.sleep(15)

	# find the "access token" geenrator and click
	el_access_click = browser.find_by_id('edit-submit-owner-token')
	for el in el_access_click:
		el.click()

	print("long nap")
	time.sleep(15)

	# extract all of the keys/tokens/whatever
	el_consumer_key = browser.find_by_css('.app-settings').find_by_css('.row').first.find_by_tag('span')[1]
	print(el_consumer_key.text)
	result['consumer_key'] = el_consumer_key.text

	el_consumer_secret = browser.find_by_css('.app-settings').find_by_css('.row')[1].find_by_tag('span')[1]
	print(el_consumer_secret.text)
	result['consumer_secret'] = el_consumer_secret.text

	el_access_key = browser.find_by_css('.access').find_by_css('.row').first.find_by_tag('span')[1]
	print(el_access_key.text)
	result['access_key'] = el_access_key.text

	el_access_secret = browser.find_by_css('.access').find_by_css('.row')[1].find_by_tag('span')[1]
	print(el_access_secret.text)
	result['access_secret'] = el_access_secret.text

	# quit chrome
	browser.quit()

	#return
	return result
