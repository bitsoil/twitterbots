import tweepy
import numpy as np
import pandas as pd
import random

import sys
import datetime
import time
import tweepy
import codecs
import csv
import shutil
import urllib
from random import randint

from datetime import datetime

# authorise api
consumer_key = 'bQwmuNToFenj708HVnq1o4wue'
consumer_secret = 'v6t0NmOsnApn478y0uZjSi8fGX31Uh5mcPdJfaVwMJBDuL1kOj'
access_token = '978925002119680000-C9kxlPMO7YfBFg2V3JLsDBGuBnImdp5'
access_token_secret = 'zDTFxJ0XRv4yU1ktglw87PrJwSrB3mJrZ4Mpsk87b3MTN'
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
twapi = tweepy.API(auth)

#variables unique to each script

img_path = '/Users/cst/Desktop/screen.jpg'



# read data/tweets.csv and bring all of the information into our script
def read_data():
	# import bot_account csv file from TAP
	print("reading data")

	df = pd.read_csv('prospector/data/tweets.csv', index_col='id')
	print("read csv")
	print(df.head())
	return df



# send our tweets! 
def send_prospect_tweets():
	df = read_data()
	# for every row in the csv file
	screen_name_lst = []
	for index, row in df.iterrows():
		if row['screen_name'] not in screen_name_lst:
			screen_name_lst.append(row['screen_name'])
			print("tweeting to user %s" % row['screen_name'])

			# random introductions for us to circle through to keep tweets unique
			intro = ['hey', 'wassup', 'hi', 'how are you?']
			i_choice = random.choice(intro)

			# here is our tweet
			msg = "%s @%s! how about %s???" % (i_choice, row['screen_name'], row['keyword'])


			
			twapi.update_with_media(filename=img_path, status=msg, in_reply_to_status_id=df['tweet_id'])
			#twapi.update_status(status=msg, in_reply_to_status_id=row['tweet_id'])
			
			print("Status updated. Getting Tweet ID")

			# get the id of the tweet that we just sent, why not?
			statuses = twapi.user_timeline()
			last_tweet = statuses[0]
			tweet_id = last_tweet.id
			print(tweet_id)

			# we take a nap so that we don't tweet too fast!
			nap = randint(15, 60)
			print("Sleeping for %s seconds" % (nap))
			time.sleep(nap)
		else: 
			print("we already tweeted to this user!")


def activate_tweeting_prospectors():
	print("Starting!")
	result = send_prospect_tweets()
	print("We're all done!!!")

if __name__ == "__main__":
	activate_tweeting_prospectors()
