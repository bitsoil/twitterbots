import tweepy
from time import sleep
from credentials import *

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)



# Tweet a line every 15 minutes
def tweet():
	file_lines = 'data/test_tweets'
	f = open(file_lines)  
	line = f.readline()
    while line: 
        try:
             print(line)
             if line != '\n':
                 api.update_status(line)
                 sleep(900)
             else:
                pass
        except tweepy.TweepError as e:
            print(e.reason)
            sleep(2)
    f.close()

if __name__ == "__main__":
	tweet()