import sys
import datetime
import time
import tweepy
import codecs
import csv
import random
import shutil
import urllib
from random import randint

creds = ['consumerkey','consumersecret','accesskey','accesssecret']
pth = '/Path/to/image'


def random_message(fle):
	line = next(fle)
	for num, aline in enumerate(fle):
	  if random.randrange(num + 2): continue
	  line = aline
	if line != '\n':
		return line
	else: 
		random_message(fle)

def retrieve_introduction():
	fle = 'data/introduction.txt'
	line = random_message(fle)
	return line


def retrieve_status_message():
	intro = retrieve_introduction()
	fle = 'data/keywords.txt'
	line = random_message(fle)
	msg = intro + line
	return msg
	
def retrieve_tweet():
	fle = 'data/tweets.txt'
	line = random_message(fle)
	return line



# authorise on api with credentials (and proxy is set here)
def auth_api():
	auth = tweepy.OAuthHandler(creds[0], creds[1])
	auth.set_access_token(creds[2], creds[3])
	twapi = tweepy.API(auth)
	twapi.proxy = proxy
	return twapi

# in order to tweet on the api, our tweet has to be in RESPONSE to something, it can't just be directed at the user
# so, we go to the "target" user's timeline and get their last tweet, returning the id. 
# We will direct our tweet at this id 
def get_last_status(twapi, target):
	status = ''
	statuses = twapi.user_timeline(screen_name=target)
	status = statuses[0]
	status_id = status.id
	return status_id

# The bot can only tweet if it's alive. We make it send a short, randdomly assmbled message in order to verify
# that it is working. 
def verify_bot():
	try:
		message = retrieve_status_message()
		twapi = auth_api()
		# tweet is sent here: 
		twapi.update_status(status=message)
		print("Success!")
		return True
	except: 
		return False 

# With a verified bot, the tweet we want to send, and a proxy, we sent our tweet to the target user
def create_tweet_and_send(usr):
	message = receive_tweet()
	preamble = retrieve_introduction()

	msg = preamble + ' @' + usr + ' ' + message

	twapi = auth_api(creds, proxy)
	status = get_last_status(twapi, target)

	# tweet is sent here
	twapi.update_with_media(filename=pth, status=msg, in_reply_to_status_id=status)
	print("Status updated. Getting Tweet ID")

	statuses = twapi.user_timeline()
	last_tweet = statuses[0]
	tweet_id = last_tweet.id
	print(tweet_id)
	return tweet_id

# With a verified bot and our assigned tweet, we attempt to tweet to the user
def send_tweet_to_user():
	try: 
		tweet_id = create_tweet_and_send()
		return tweet_id
	except: 
		return "FAIL"


if __name__ == "__main__":
	print("hello!")
	usr = input("What's your name? ")
	res = verify_bot()
	if res: 
		send_tweet_to_user(usr)